#  R packaging run-time environment
#  Copyright (C) 2022 Victor Santos
#
#  License: GPL-3.0+

FROM r-base:latest
LABEL author.name="Victor Santos" \
      author.email="vct.santos@protonmail.com"

RUN apt-get update -qq && \
    apt-get -y --no-install-recommends install \
    libssl-dev \
    libcurl4-openssl-dev \
    libssh2-1-dev \
    libxml2-dev \
    libfontconfig1-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libfreetype6-dev \
    libpng-dev \
    libtiff5-dev \
    libjpeg-dev \
  && \
    install2.r --error \
    devtools \
    roxygen2 \
  && \
    rm -rf /tmp/downloaded_packages
