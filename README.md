# r-packaging

A [Docker](https://www.docker.com/) container for building [R](https://www.r-project.org/) packages, intended for use in [Continuous Integration](https://about.gitlab.com/features/continuous-integration/) system of [Gitlab](https://gitlab.com/). 

This image is based on [rocker/r-base](https://hub.docker.com/_/r-base) image.
